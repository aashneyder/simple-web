# FlaskBlog

Простой веб-сайт для демонстрации работы с [getsops](https://getsops.io/docs/)

# Инструкция

**1. Шифрование на локальном хосте**

- Генерация ключей

```age-keygen -o key.txt```

- Создание папки для хранения ключей и перенос файла с ключами в нее

``` mkdir $HOME/.sops && mv ./key.txt $HOME/.sops/key.txt```

- Экспорт пути до ключей в переменную окружения

```export SOPS_AGE_KEY_FILE=$HOME/.sops/key.txt```

- Шифрование файла

```
sops --encrypt --age $(cat $SOPS_AGE_KEY_FILE | grep -oP "public key: \K(.*)") --in-place ./.env
```

**2. Расшифровка на другом хосте (после клонирования репозитория)***
- Установка sops и age

```
curl -LO https://github.com/getsops/sops/releases/download/v3.9.0/sops-v3.9.0.linux.amd64
sudo mv sops-v3.9.0.linux.amd64 /usr/local/bin/sops
chmod +x /usr/local/bin/sops
sudo apt install age
```

- Создание папки под ключ и перенос ключей

```
mkdir $HOME/.sops && vi $HOME/.sops/key.txt 
>> copy paste your sops keys
```

- Экспорт пути до ключа в переменную окружения

```export SOPS_AGE_KEY_FILE=$HOME/.sops/key.txt```

- Дешифровка файла

```
sops --decrypt --age $(cat $SOPS_AGE_KEY_FILE | grep -oP "public key: \K(.*)") --in-place ./.env
```
